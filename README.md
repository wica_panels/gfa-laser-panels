# Overview 

This is the **gfa-laser-panels** git repository. 

This repository uses web components created by the [gfa-laser-wc-factory](https://git.psi.ch/controls_highlevel_applications/gfa-laser-wc-factory)

# Project Changes and Tagged Releases

* See the [CHANGELOG.md](CHANGELOG.md) file for further information.
